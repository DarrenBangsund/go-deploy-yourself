package main

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"sync"

	"github.com/nxtlvlgg/tempo-deploy/core"
	"github.com/nxtlvlgg/tempo-deploy/message"
	"github.com/nxtlvlgg/tempo-deploy/models"
)

func sendPkg(con *models.SSHConfig, file string) *models.RoutineErr {
	home := os.Getenv("HOME")
	keystr := fmt.Sprintf("%s%s.ppk", home, con.Key)
	userHost := fmt.Sprintf("%s@%s:", con.User, con.IP)
	executable := "pscp.exe"

	if _, err := exec.LookPath(executable); err != nil {
		message.Print("w", fmt.Sprintf("pscp is not available in your PATH, please install it and set it in PATH\n"))
		return models.NewError(err, con)
	}

	cmd := exec.Command(executable, "-i", keystr, file, userHost)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Start()
	cmd.Wait()
	return nil
}

func cleanEmpty(arr []string) []string {
	for i, v := range arr {
		if v == "" {
			arr[i] = arr[len(arr)-1]
			arr = arr[:len(arr)-1]
		}
	}

	return arr
}

var (
	configLoc string
	confsin   models.Conf
	// debug     *bool
	fi []string
)

func main() {
	os.Exit(realMain())
}

func realMain() int {
	// debug = flag.Bool("d", false, "Debug Mode")
	flag.Parse()

	arg := os.Args[1]
	if err := confsin.Set(arg, &configLoc); err != nil {
		// e <- err
		return 1
	}

	e := make(chan *models.RoutineErr, 0)
	results := make(chan string)

	//Check the target current checkedout branch
	var br bytes.Buffer
	branch := core.ExecCmd("git", confsin.Dir.UploadFrom, &br, "rev-parse", "--abbrev-ref", "HEAD")
	branch.Run()

	if strings.TrimSpace(br.String()) != confsin.Dir.Branch {
		message.Print("w", fmt.Sprintf("Looks like you're trying to deploy from a branch other than %s (%s), exiting.", confsin.Dir.Branch, strings.TrimSpace(br.String())))
		return 1
	}

	//check for uncommitted changes
	var ch bytes.Buffer
	changes := core.ExecCmd("git", confsin.Dir.UploadFrom, &ch, "diff-index", "HEAD")
	err := changes.Run()
	fmt.Println(ch.String())
	arr := cleanEmpty(strings.Split(ch.String(), "\n"))

	if len(arr) != 0 {
		message.Print("w", fmt.Sprintf("It looks like you have some uncommitted changes in your branch %s, please commit those changes and try again.", confsin.Dir.Branch))
		return 1
	}

	// core.GetDiff(&confsin)

	message.Print("i", "Executing pre commands locally...")
	err = core.ExecMany(confsin.Dir.UploadFrom, confsin.Dir.Pre)
	if err != nil {
		message.Print("e", fmt.Sprintf("There was an error executing one of your pre commands: %v", err))
		return 1
	}
	message.Print("o", "Execution successful!")
	message.Print("o", fmt.Sprintf("Total executed: %v\n", len(confsin.Dir.Pre)))

	//Build the file tree from the current commit hash
	message.Print("i", "Building file tree...")
	tree := core.GetTree(&confsin)
	if tree == nil {
		return 1
	}

	//Git likes to return an empty filename which will throw panics, this cleans it out
	fi = cleanEmpty(tree)
	message.Print("o", "File tree successfully built!")
	message.Print("o", fmt.Sprintf("Total files to zip: %v\n", len(fi)))

	//Zip our files
	message.Print("i", fmt.Sprintf("Beginning zip of dir: %s", confsin.Dir.UploadFrom))
	buf, err := core.ZipFiles(fi, confsin.Dir.UploadFrom)
	if err != nil {
		panic(err)
	}
	message.Print("o", "Zip successful!\n")

	//Generate our file name by md5 hash of our zip contents
	zipHash := fmt.Sprintf("%x", md5.Sum(buf))
	file := fmt.Sprintf("%s\\%s.zip", os.Getenv("TEMP"), zipHash)
	if err := ioutil.WriteFile(file, buf, 0666); err != nil {
		fmt.Print(err)
		return 1
	}

	var cluster = make([][]*models.SSHConfig, confsin.Cluster)
	for i, v := range confsin.SSH {
		idx := i % confsin.Cluster
		arr := cluster[idx]

		cluster[idx] = append(arr, v)
	}

	parallel := 1
	cpus := runtime.NumCPU()
	if cpus > 1 {
		parallel = cpus - 1
	}

	if parallel < confsin.Cluster {
		parallel = confsin.Cluster
	}

	canexit := make(chan bool)
	msglock := sync.WaitGroup{}
	go func() {
		er := false

		for i := 0; i < len(confsin.SSH); i++ {
			select {
			case err := <-e:
				msglock.Add(1)
				message.Print("e", fmt.Sprintf("Something went wrong with connection %s", err.SSH.Name))
				message.Print("e", fmt.Sprintf("%v", err.Message))
				message.Print("e", fmt.Sprint("SSH connection has been terminated and no files will be uploaded..\n"))
				er = true
			case res := <-results:
				msglock.Add(1)
				message.Print("o", fmt.Sprintf("%s", res))

				message.Print("i", "Executing post commands locally...")
				core.ExecMany(confsin.Dir.UploadFrom, confsin.Dir.Post)
				message.Print("o", "Execution successful!")
				message.Print("o", fmt.Sprintf("Total executed: %v\n", len(confsin.Dir.Post)))
			}
			msglock.Done()
		}

		if !er {
			message.Print("f", "No errors occured, writing config")

			var byt bytes.Buffer
			cmd := core.ExecCmd("git", confsin.Dir.UploadFrom, &byt, "rev-parse", "HEAD")
			cmd.Run()
			confsin.Dir.Latest = strings.TrimSpace(byt.String())

			b, _ := json.Marshal(confsin)
			ioutil.WriteFile(configLoc, b, 0644)
		}

		if err := os.Remove(file); err != nil {
			message.Print("w", fmt.Sprintf("There was an error removing the zip file from your local machine"))
			message.Print("w", fmt.Sprintf("Please check: %s\n", file))
		}

		canexit <- true
	}()

	for _, v := range cluster {
		var wg = sync.WaitGroup{}
		clustLen := len(v)
		if parallel > clustLen {
			parallel = clustLen
		}

		semaphore := make(chan int, parallel)
		wg.Add(clustLen)

		//Start the threads for uploading to each server
		for _, profile := range v {
			go func(con *models.SSHConfig) {
				defer wg.Done()

				semaphore <- 1

				var err *models.RoutineErr

				//Upload the package
				message.Print("i", fmt.Sprintf("Beginning upload to %s...", con.Name))
				if err := sendPkg(con, file); err != nil {
					e <- err
					return
				}

				//Init our ssh connection
				session, err := models.NewSSHSession(con)
				if err != nil {
					e <- err
					return
				}

				// Begin pre unpack commands
				for _, cmd := range con.Pre {
					err = session.SSHRun(cmd)
					if err != nil {
						e <- err
						return
					}
				}

				//Unpack our package to target directory
				err = session.VerifyRemoteDir(confsin.Dir.UploadTo)
				if err != nil {
					e <- err
					return
				}

				err = session.SSHRun(fmt.Sprintf("unzip -o %s -d %s", zipHash, confsin.Dir.UploadTo))
				if err != nil {
					e <- err
					return
				}

				// for _, v := range strings.Split(string(unzipped), "\n") {
				// 	message.Print("d", fmt.Sprintf("%s", v))
				// }
				// message.Print("o", "Unpacking on remote host successful!\n")

				//Begin post unpack commands
				for _, cmd := range con.Post {
					err = session.SSHRun(cmd)
					if err != nil {
						e <- err
						message.Print("e", " ")
						return
					}
				}

				//Clean up our zip
				err = session.SSHRun(fmt.Sprintf("rm -rf %s.zip", zipHash))
				if err != nil {
					e <- err
					message.Print("e", " ")
					return
				}

				results <- fmt.Sprintf("%s finished upload and is now exiting\n", con.Name)
				msglock.Wait()

				<-semaphore

			}(profile)
		}

		wg.Wait()
	}

	<-canexit
	return 0
}
