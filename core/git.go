package core

import (
	"bytes"
	"fmt"
	"sort"
	"strings"

	zglob "github.com/mattn/go-zglob"
	"github.com/nxtlvlgg/tempo-deploy/message"
	"github.com/nxtlvlgg/tempo-deploy/models"
)

func getFullTree(path string) []string {
	var out bytes.Buffer
	cmd := ExecCmd("git", path, &out, "ls-tree", "--name-only", "-r", "HEAD")
	cmd.Run()

	return strings.Split(out.String(), "\n")
}

func GetDiff(conf *models.Conf) []string {
	var out bytes.Buffer
	diff := ExecCmd("git", conf.Dir.UploadFrom, &out, "diff", "--name-only", conf.Dir.Latest)
	if err := diff.Run(); err != nil {
		panic(err)
	}

	temp := strings.Split(out.String(), "\n")

	if len(temp) == 1 {
		return nil
	}

	return temp
}

func GetTree(conf *models.Conf) []string {
	hash := conf.Dir.Latest
	var diff []string

	if hash != "" {
		//If the hash is not empty then we build the file tree from the git hash
		diff := GetDiff(conf)

		if diff == nil {
			message.Print("f", fmt.Sprintf("%s", "Already up to date"))
			return nil
		}
	} else {
		//If hash is empty then we upload the full branch
		diff = getFullTree(conf.Dir.UploadFrom)
	}

	var globs []string
	var idx []int

	//find glob patterns
	for _, v := range conf.Dir.Ignore {
		matches, _ := zglob.Glob(fmt.Sprintf("%s\\%s", conf.Dir.UploadFrom, v))

		for _, m := range matches {
			globs = append(globs, m)
		}
	}

	//generate an index array of files that matched globs, to remove
	for _, g := range globs {
		for i, t := range diff {
			str := strings.Replace(fmt.Sprintf("%s\\%s", conf.Dir.UploadFrom, t), "/", "\\", -1)

			if strings.EqualFold(strings.Replace(g, "/", "\\", -1), str) {
				idx = append(idx, i)
			}
		}
	}

	//sort the index array
	sort.Ints(idx)

	//reverse index array
	for i := len(idx)/2 - 1; i >= 0; i-- {
		opp := len(idx) - 1 - i
		idx[i], idx[opp] = idx[opp], idx[i]
	}

	//remove indices from our files list
	for _, id := range idx {
		diff = append(diff[:id], diff[id+1:]...)
	}

	return diff
}
