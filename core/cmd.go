package core

import (
	"bytes"
	"os"
	"os/exec"
	"strings"

	"github.com/nxtlvlgg/tempo-deploy/message"
)

func ExecCmd(command string, dir string, buf *bytes.Buffer, flags ...string) (cmd *exec.Cmd) {
	cmd = exec.Command(command, flags...)
	cmd.Dir = dir

	if buf == nil {
		cmd.Stdout = os.Stdout
	} else {
		cmd.Stdout = buf
	}

	return
}

func ExecMany(uploadFrom string, cmd []string) error {
	for _, v := range cmd {
		message.Print("i", v)

		str := strings.Split(v, " ")
		x, a := str[0], str[1:]

		cmd := ExecCmd(x, uploadFrom, nil, a...)
		err := cmd.Run()
		if err != nil {
			return err
		}
	}

	return nil
}
