package core

import (
	"archive/zip"
	"bytes"
	"compress/flate"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/nxtlvlgg/tempo-deploy/message"
)

func ZipFiles(files []string, dir string) ([]byte, error) {
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)
	w.RegisterCompressor(zip.Deflate, func(out io.Writer) (io.WriteCloser, error) {
		return flate.NewWriter(out, flate.BestCompression)
	})

	for _, file := range files {
		fi, err := ioutil.ReadFile(fmt.Sprintf("%s\\%s", dir, file))
		if err != nil {
			return nil, err
		}

		f, err := w.Create(file)
		if err != nil {
			return nil, err
		}

		message.Print("d", fmt.Sprintf("Zipping %s...", file))
		_, err = f.Write(fi)
		if err != nil {
			return nil, err
		}
	}

	err := w.Close()
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
