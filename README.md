# go-deploy-yourself #
Simple deploy tool written in Go. Supports multi-threaded deployment to multiple servers in clusters to support graceful restarting.

## Required ##
PSCP is required to upload files to the remote server. I chose pscp for the ability to use .PPK files without having to generate seperate openSSH keys. After downloading and installing, make sure PSCP is accessable in your PATH env var. An error will occur otherwise.

Download: http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html


## Configuring ##
Configuration 

```json
{
    "clusterSize": 1, (**see below)
    "ssh": [
        {
            "name": "server1",
            "user": "username",
            "ip": "555.5.55.5",
            "key": "path/to/your/ppk/file", (searches in %HOME%/.ssh)
            "port": "22",
            "pre": [
                "echo before unzip!" (this will be executed on your remote server)
            ],
            "post": [
                "echo post unzip!" (this will be executed on your remote server)
            ]
        },
        {
            "name": "server2",
            "user": "username2",
            "ip": "123.4.56.9",
            "key": "path/to/your/ppk/file", (searches in %HOME%/.ssh)
            "port": "22",
            "pre": [],
            "post": []
        }
    ],
    "dir": {
        "uploadFrom": "absolute/path/to/your/project", (this dir needs a .git project)
        "uploadTo": "path/to/upload/spot",
        "latest": "", (leave this empty for your first deploy, this will get automatically generated)
        "pre": [
            "echo hello world" (before anything packaging happens these commands are executed on your local machine)
        ],
        "post": [
            "echo done!" (these commands are executed on your local machine AFTER everything has uploaded)
        ],
        "branch": "master" (this can be any branch you wish to deploy from eg. staging)
    }
}
```

** go-deploy-yourself supports clustering, how this works is very simple. Suppose you have the above config deploying to two servers, *server1* and *server2*, if you define *clusterSize* as 1 then the program
will assume no clustering and upload to those servers concurrently, however, if you define *clusterSize* to 2 then the program will assume the servers are clustered together and will not start uploading to *server2*
until the first is completely finished. 

Suppose you have 4 servers with a cluster size of 2 giving you a total of 2 clusters.
```
Cluster1.serverA
Cluster1.serverB

Cluster2.serverA
Cluster2.serverB
```
The program will upload your package to *Cluster1.serverA* and *Cluster2.serverA* concurrently, and will wait for every serverA in Cluster*N* to be finished before moving onto serverB.

If you are using clustering it is **very** important that your servers are in ascending order by cluster in the json config.
```
Cluster1.serverA
Cluster1.serverB
Cluster2.serverA
Cluster2.serverB
...
ClusterN.serverA
ClusterN.serverB
ClusterN+1.serverA
ClusterN+1.serverB
```

Clustering can support up to N+1 servers.
```
Cluster1.serverA
...
Cluster1.serverN
Cluster1.serverN+1

Cluster2.serverA
...
Cluster2.serverN
Cluster2.serverN+1
```

## How to use ##
Create a configuration as above in `%APPDATA%/nxtlvlgg/go-deploy-yourself/config/CONFIGNAME.json`

Once you're all configured you can run the program from cmd trailing with `CONFIGNAME` eg. `go-deploy-yourself.exe CONFIGNAME`

## TODO ##

* Generation tool for configs
* More graceful error handling
* GUI?

## Known issues ##

* JSON config becomes unformatted and hard to read after hash generation