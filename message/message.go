package message

import (
	"fmt"

	"github.com/fatih/color"
)

type message struct {
	msg string
	t   string
}

func Print(t string, formatted string) {
	i := color.New(color.FgBlue).SprintfFunc()
	w := color.New(color.FgYellow).SprintfFunc()
	e := color.New(color.FgHiRed).SprintfFunc()
	s := color.New(color.FgGreen).SprintfFunc()
	f := color.New(color.FgHiGreen).SprintfFunc()
	// d := color.New(color.BgYellow, color.FgBlack).SprintfFunc()

	switch t {
	case "i":
		fmt.Printf("[%s] %s\n", i("INFO"), formatted)
	case "w":
		fmt.Printf("[%s] %s\n", w("WARN"), formatted)
	case "e":
		fmt.Printf("[%s] %s\n", e("ERROR"), formatted)
	case "o":
		fmt.Printf("[%s] %s\n", s("OK"), formatted)
	case "f":
		fmt.Printf("[%s] %s\n", f("COMPLETE"), formatted)
		// case "d":
		// if *debug {
		// fmt.Printf("[%s] %s\n", d("DEBUG"), formatted)
		// }
	}
}
