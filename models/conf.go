package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/nxtlvlgg/tempo-deploy/message"
)

const (
	COMPANY = "nxtlvlgg"
	NAME    = "go-deploy-yourself"
)

type Conf struct {
	Cluster int          `json:"clusterSize"`
	SSH     []*SSHConfig `json:"ssh"`
	Dir     Dir          `json:"dir"`
}

func (c *Conf) String() string {
	return fmt.Sprint(*c)
}

func (c *Conf) Set(value string, configLoc *string) error {
	app := os.Getenv("APPDATA")
	dir := fmt.Sprintf("%s\\%s\\%s\\config", app, COMPANY, NAME)
	err := os.MkdirAll(dir, os.ModePerm)
	// wd, err := os.Getwd()
	if err != nil {
		message.Print("e", fmt.Sprintf("Cannot create directory: %s", err))
		return err
	}
	ex := fmt.Sprintf("%s\\example.json", dir)
	str := fmt.Sprintf("%s\\%s.json", dir, value)
	*configLoc = str

	// if strings.Contains(value, ":/") {
	// 	str = value
	// }

	_, err = ioutil.ReadFile(ex)
	if err != nil {
		message.Print("w", fmt.Sprintf("Example config does not exist: %s", err))
	}

	dat, err := ioutil.ReadFile(str)
	if err != nil {
		message.Print("e", fmt.Sprintf("Cannot open file: %s", err))
		return err
	}

	var d Conf

	json.Unmarshal(dat, &d)
	if err != nil {
		message.Print("e", fmt.Sprintf("JSON Marshal failed: %s", err))
		return err
	}

	*c = d

	return nil
}
