package models

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/nxtlvlgg/tempo-deploy/message"

	"golang.org/x/crypto/ssh"
)

type SSHSession struct {
	client *ssh.Client
	con    *SSHConfig
}

func NewSSHSession(conf *SSHConfig) (*SSHSession, *RoutineErr) {
	loc := fmt.Sprintf("%s%s", os.Getenv("HOME"), conf.Key)
	key, err := ioutil.ReadFile(loc)
	if err != nil {
		return nil, NewError(err, conf)
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return nil, NewError(err, conf)
	}

	clientConfig := &ssh.ClientConfig{
		User: conf.User,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
	}

	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", conf.IP, conf.Port), clientConfig)
	if err != nil {
		return nil, NewError(err, conf)
	}

	return &SSHSession{client: client, con: conf}, nil
}

func (s *SSHSession) SSHRun(command string) *RoutineErr {
	var err error

	// print("d", command)

	session, err := s.client.NewSession()
	if err != nil {
		return NewError(err, s.con)
	}

	// session.Stdout = os.Stdout
	session.Stderr = os.Stderr

	defer session.Close()

	err = session.Start(command)
	if err != nil {
		// print("e", fmt.Sprintf("Command(s) failed %s", err))
		return NewError(err, s.con)
	}

	if err := session.Wait(); err != nil {
		// print("e", fmt.Sprintf("Session wait failed: %s %s", command, err))
		return NewError(err, s.con)
	}

	return nil
}

func (s *SSHSession) VerifyRemoteDir(loc string) *RoutineErr {
	cmd := fmt.Sprintf("ls %s", loc)
	if err := s.SSHRun(cmd); err == nil {
		return nil
	}

	message.Print("w", fmt.Sprintf("%s does not exist, attempting to create", loc))
	err := s.SSHRun(fmt.Sprintf("mkdir -p %s", loc))
	if err == nil {
		message.Print("o", fmt.Sprintf("Dir was successfully created"))
		return nil
	}

	return err
}
