package models

type Dir struct {
	UploadFrom string   `json:"uploadFrom"`
	UploadTo   string   `json:"uploadTo"`
	Latest     string   `json:"latest"`
	Pre        []string `json:"pre"`
	Post       []string `json:"post"`
	Branch     string   `json:"branch"`
	Ignore     []string `json:"ignore"`
	toUpload   []string
	toRemove   []string
}
