package models

type RoutineErr struct {
	Message error
	SSH     *SSHConfig
}

func NewError(err error, con *SSHConfig) *RoutineErr {
	return &RoutineErr{Message: err, SSH: con}
}
