package models

type SSHConfig struct {
	Name string   `json:"name"`
	User string   `json:"user"`
	IP   string   `json:"ip"`
	Key  string   `json:"key"`
	Port string   `json:"port"`
	Pre  []string `json:"pre"`
	Post []string `json:"post"`
}
